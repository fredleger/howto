# Kube And Run - Kubernetes based bootcamp

## Prerequisite : Helm init

First RBAC preparation :

```bash
kubectl -n kube-system create sa tiller
kubectl create clusterrolebinding tiller \
    --clusterrole cluster-admin --serviceaccount=kube-system:tiller
helm init --service-account tiller
```

## Logging (Fluentd/Elastic/Kibana)

```bash
helm repo add bitnami https://charts.bitnami.com/bitnami

helm install --namespace monitoring --name monitoring-elasticsearch \
    --set name=cluster-logs,client.service.port=9200,sysctlImage.enabled=true \
    bitnami/elasticsearch
  
kubectl -n monitoring expose --name elasticsearch \
    deploy/monitoring-elasticsearch-elasticsearch-coordinating-only

helm install --namespace monitoring --name monitoring-kibana \
    --set service.externalPort=5601 \
    stable/kibana

helm install --namespace monitoring --name monitoring-fluentd \
    --set elasticsearch.host=elasticsearch \
    stable/fluentd-elasticsearch
```

Access to Kibana : `kubectl port-forward svc/monitoring-kibana 5601` and browse http://127.0.0.1:5601

## Metrology (Prometheus/Grafana)

```bash
helm install --namespace monitoring --name prometheus \
    stable/prometheus
    
helm install --namespace monitoring --name grafana \
    --set persistence.enabled=true,persistence.accessModes={ReadWriteOnce} \
    --set persistence.size=8Gi \
    stable/grafana
```

Access to grafana : `kubectl port-forward svc/grafana 8080:8` and browse http://127.0.0.1:8080

User is `admin` and password can be retrieved with command: 

```bash
kubectl get secret --namespace monitoring grafana \
    -o jsonpath="{.data.admin-password}" | base64 --decode ; echo
```

You can add Prometheus server as source with url http://prometheus-server

## Weavescope

```bash
helm install --namespace monitoring --name weave-scope stable/weave-scope
```

Access to Weave Scope UI : `kubectl port-forward svc/weave-scope-weave-scope 8080:80`

## Docker garbage collector

```bash
helm install --namespace ops --name docker-gc stable/spotify-docker-gc
```

